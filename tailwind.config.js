const colors = require('tailwindcss/colors');

module.exports = {
  mode: 'jit',
  purge: [
    './public/**/*.html',
    './src/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
        ...colors,
        'primary': {
          DEFAULT: '#003592',
          '50': '#D5E6FB',
          '100': '#367FFF',
          '200': '#0D65FF',
          '300': '#0053E4',
          '400': '#0044BB',
          '500': '#003592',
          '600': '#00215A',
          '700': '#000C22',
        },
        'secondary': {
          DEFAULT: '#F53838',
        },
        'primaryText': {
          DEFAULT: '#092C4C',
        },
        'secondaryText': {
          DEFAULT: '#27153E',
        },
        'success': {
          DEFAULT: '#2FBB56',
        },
        'secondaryGrey': {
          DEFAULT: '#E0E0E0',
          '3': '#E0E0E0',
          '4': '#BDBDBD',
          '5': '#828282',
        },
    },
    extend: {
      fontFamily: {
          sans: ['Poppins'],
      },
    },
  },
  variants: {
    extend: {
      scale: ['group-hover', 'hover']
    },
  },
  plugins: [],
}
